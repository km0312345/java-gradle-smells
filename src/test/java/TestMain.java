import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class TestMain {


    @Test
    public void countPrice() {

        Starter main = new Starter();

        assertEquals(12,
                main.countPrice(
                Starter.Ingredients.THICK_CRUNCH,
                Starter.Ingredients.CHILI_SAUCE,
                Starter.Ingredients.MOZZARELLA),
                "(2+1) + 4 + 5");

        assertEquals(21,
                main.countPrice(
                        Starter.Ingredients.THIN_CRUNCH,
                        Starter.Ingredients.WHITE_SAUCE,
                        Starter.Ingredients.HOT_SAUCE,
                        Starter.Ingredients.FETA),
                "2 + 4 + 7 + 8");

    }
}
