public class white_sauce implements sauce {

    @Override
    public int getPrice() {
        return 4;
    }

    @Override
    public void initSpiciness() {
    }

    public int add_crunch(crunch.thin_crunch crunch) {
        return getPrice() + crunch.price;
    }

    public int add_crunch(crunch.thick_crunch crunch) {
        return getPrice() + crunch.price;
    }
}