import java.util.*;
import java.lang.*;

public class chili_sauce extends Object implements sauce {

    public int spiciness;

    @Override
    public int getPrice() {
        return 4;
    }

    @Override
    public void initSpiciness() {
        spiciness = 2;
    }

    public int add_crunch(crunch.thin_crunch crunch) {
        return getPrice() + crunch.price;
    }

    public int add_crunch(crunch.thick_crunch crunch) {
        return getPrice() + crunch.price;
    }

    @Override
    public int hashCode() {
        return Objects.hash(spiciness);
    }
}